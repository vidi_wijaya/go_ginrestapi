package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"teknoark.com/model"
)

type MahasiswaInput struct {
	Nim  string `json:"nim"`
	Nama string `json:"nama"`
}

//show all data
func Show(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var mhs []model.Mahasiswa
	db.Find(&mhs)
	c.JSON(http.StatusOK, gin.H{"data": mhs})
}

//show data by id
func ShowById(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	//search data in db
	var mhs model.Mahasiswa
	if err := db.Where("nim = ?", c.Param("nim")).First(&mhs).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Data Tidak Ditemukan."})
		return
	}
	db.Find(&mhs)
	c.JSON(http.StatusOK, gin.H{"data": mhs})
}

//add data
func Add(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	//validasi input
	var dataInput MahasiswaInput
	if err := c.ShouldBindJSON(&dataInput); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//input data
	mhs := model.Mahasiswa{
		Nim:  dataInput.Nim,
		Nama: dataInput.Nama,
	}

	db.Create(&mhs)

	c.JSON(http.StatusOK, gin.H{"data": mhs})

}

//update data
func Update(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	//search data in db
	var mhs model.Mahasiswa
	if err := db.Where("nim = ?", c.Param("nim")).First(&mhs).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Data Tidak Ditemukan."})
		return
	}

	//validasi input
	var dataInput MahasiswaInput
	if err := c.ShouldBindJSON(&dataInput); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//update data
	db.Model(&mhs).Update(dataInput)

	c.JSON(http.StatusOK, gin.H{"data": mhs})

}

//delete data
func Delete(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	//search data in db
	var mhs model.Mahasiswa
	if err := db.Where("nim = ?", c.Param("nim")).First(&mhs).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Data Tidak Ditemukan."})
		return
	}

	//delete data
	db.Delete(&mhs)

	c.JSON(http.StatusOK, gin.H{"data": true})

}
