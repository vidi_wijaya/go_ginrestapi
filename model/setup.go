package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func SetupModel() *gorm.DB {
	db, err := gorm.Open("mysql", "root:@(localhost)/gin_restapi?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		panic("gagal koneksi database")
	}

	db.AutoMigrate(&Mahasiswa{}) //buat database otomatis
	return db
}
